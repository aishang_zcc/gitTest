<?php 
    // create curl resource 创建了一个curl会话资源，成功返回一个句柄
   $ch = curl_init(); 
 
   // set url 设置URL
   // curl_setopt($ch, CURLOPT_URL, "baidu.com"); 
   curl_setopt($ch, CURLOPT_URL, "https://github.com/search?q=react"); 
 
   //return the transfer as a string 这是设置是否将响应结果存入变量，1是存入，0是直接echo出；
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
 
   // $output contains the output string  执行，然后将响应结果存入$output变量，供下面echo
   $output = curl_exec($ch); 
 
    //echo output
    echo $output;
 
   // close curl resource to free up system resources  关闭这个curl会话资源
   curl_close($ch);      
?>