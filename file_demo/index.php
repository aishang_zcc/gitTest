<?php
	//相对路径
	//$contract = 'son/son.pdf';//相当于入口文件所在目录下 son 中的abc.pdf
	//$contract = './son/son.pdf';//相当于入口文件所在目录下 son 中的abc.pdf 和son/abc.pd 一样
	$contract = "abc.pdf";//当前文件（或入口）所在目录下
	$contract = "./abc.pdf";//当前文件（或入口）所在目录下
	$contract = "../test.txt";//当前文件（或入口）上一级目录下
	//绝对路径
	$contract = "";
	$contract = "/test.txt"; //以/开头通常代表从根目录开始
	$contract = './测试.xlsx'; //不支持 中文名字
	$filename=iconv('UTF-8','GB2312',$contract);
	$file = file_exists($filename);
	//$content = file_get_contents($contract);
	echo "文件是否存在：";
	var_dump($file);
	//echo "<br>内容为$content";
	
	$imgurl = 'http://img.hzc.com/hzc/photo/201601/20160119_C4A23206D8579AC09090BDAF6D0B8FA0.jpg';//filesize无法使用外部链接 来获取文件大小
	//$imgurl = 'a.png';
	//$size = filesize($imgurl);
	//echo "<br>".$size;
	
	
	$url="http://www.somewhere.org/index.htm";
    if (file_exists($url)) echo "Wow!\n";
    else echo "<br>missing\n";
	
	$url="http://img.hzc.com/hzc/photo/201601/20160119_C4A23206D8579AC09090BDAF6D0B8FA0.jpg";
    if (false!==file($url)) echo "Wow!\n";
    else echo "missing\n";
	

	
	//初始化一个cURL会话
	$ch = curl_init();

	//设定请求的RUL
	curl_setopt($ch, CURLOPT_URL, 'www.baidu.com');

	//设定返回信息中包含响应信息头
	curl_setopt($ch, CURLOPT_HEADER, 1);// 	启用时会将头文件的信息作为数据流输出。 
	//参数为1表示输出信息头,为0表示不输出

	curl_setopt($ch,CURLOPT_NOPROGRESS,0);

	//设定curl_exec()函数将响应结果返回，而不是直接输出
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);//TRUE 将curl_exec()获取的信息以字符串返回，而不是直接输出。 
	//参数为1表示$html,为0表示echo $html
	


	//执行一个cURL会话
	$html = curl_exec($ch);

	//关闭一个surl会话
	curl_close($ch);

	//输出返回信息  如果CURLOPT_RETURNTRANSFER参数为fasle可省略
	//echo $html;

		
	die;

?>