<?php
/**
* 
*/
class GetImgSize
{
	public function getinfo($url)
	{

		echo mb_substr('我们都是中国人',0,-2)."<br>";  


		$info = getimagesize($url);
        if ($info) {
            $result['width'] = $info[0];
            $result['height'] = $info[1];
            $result['size'] = strlen(file_get_contents($url));
            return $result;
        } else {
            return false;
        }
	}
}

/**
 * 获取图片尺寸工具类
 * @author Robot
 * 
 * @example
 * $result = ImageTools::getSize (
 * 	array('url'=>'https://www.baidu.com/img/bdlogo.png')	
 * );
 *	print_r ( $result );
 *
 */
class ImageTools {
	/**
     * 获取远程图片的宽高和体积大小
     *
     * @param string $url
     *        	远程图片的链接
     * @param string $type
     *        	获取远程图片资源的方式, 默认为 curl 可选 fread
     * @param boolean $withsize
     *        	是否获取远程图片的体积大小, 默认false不获取, 设置为 true 时 $type 将强制为 fread
     * @return false array
     */
    public static function getSize($paramArr) {
    	$options = array(
            'url'    => false, #远程图片连接
            'type'   => 'curl', #获取远程图片资源方式，默认为Curl,需要同时获取图片尺寸使用fread
            'withsize' => true, #是否获取远程图片的体积大小, 默认false不获取, 设置为 true 时 $type 将强制为 fread
            'failTry'  => true   #请求失败重试，有可能网络问题，第一次请求可能有失败
    	);
    	if (is_array($paramArr))$options = array_merge($options, $paramArr);
    	extract($options);
    	
    	// 若需要获取图片体积大小则默认使用 fread 方式
    	$type = $type === 'fread'?'fread':'curl';
    	$type = $withsize ? 'fread' : $type;
    	if ($type == 'fread') {
    		$handle = fopen ( $url, 'rb' ); // 或者使用 socket 二进制方式读取, 需要获取图片体积大小最好使用此方法
    		if (! $handle)
    			return false;
    		$dataBlock = fread ( $handle, 168 ); // 只取头部固定长度168字节数据
    	} else {
    		$ch = curl_init ( $url );
    		curl_setopt ( $ch, CURLOPT_TIMEOUT, 3 ); // 超时时间
    		curl_setopt ( $ch, CURLOPT_RANGE, '0-167' ); // 取前面 168 个字符
    		curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, 1 ); // 跟踪301跳转
    		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true ); // 返回结果
    		$dataBlock = curl_exec ( $ch );
    		curl_close ( $ch );
    		if (! $dataBlock)
    			return false;
    	}
    	// 将读取的图片信息转化为图片路径并获取图片信息,经测试,这里的转化设置 jpeg 对获取png,gif的信息没有影响,无须分别设置
    	// 有些图片虽然可以在浏览器查看但实际已被损坏可能无法解析信息
    	$size = getimagesize ( 'data://image/jpeg;base64,' . base64_encode ( $dataBlock ) );
    	if (empty ( $size ) && $failTry) {
    		$size = getimagesize($url);//某些图片损坏无法获取，直接使用远程连接获取一次，保证获取到正确size
    	}
    	if (empty ( $size ))
    		return false;
    	$result ['width'] = $size [0];
    	$result ['height'] = $size [1];
    	// 是否获取图片体积大小
    	if ($withsize) {
    		// 获取文件数据流信息
    		$meta = stream_get_meta_data ( $handle );
    		// nginx 的信息保存在 headers 里，apache 则直接在 wrapper_data
    		$dataInfo = isset ( $meta ['wrapper_data'] ['headers'] ) ? $meta ['wrapper_data'] ['headers'] : $meta ['wrapper_data'];
    			
    		foreach ( $dataInfo as $va ) {
    			if (preg_match ( '/length/iU', $va )) {
    				$ts = explode ( ':', $va );
    				$result ['size'] = trim ( array_pop ( $ts ) );
    				break;
    			}
    		}
    	}
    
    	if ($type == 'fread')
    		fclose ( $handle );
    
    	return $result;
    }
}

$obj = new GetImgSize();
$info = $obj->getinfo('http://img.moxingyun.com/expoimg/max/2019/1252/1912231525298578.jpg');
// $info = ImageTools::getSize (
//  	array('url'=>'http://img.moxingyun.com//expoimg/max/2019/1249/041250090606109.jpg')	
// );
print_r($info);
?>