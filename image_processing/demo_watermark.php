<?php
    // s生成带水印的图片    

    $dst_path = 'img.jpg';//图片路径
    $image_name = explode('.',basename($dst_path));
    //创建图片的实例
    $dst = imagecreatefromstring(file_get_contents($dst_path));
    //设置响应头(编码格式)
    header("Content-Type:text/html;charset=utf-8");

    $font = 'STXINGKA.ttf';//字体（必须下载字体库） 有些字体不支持中文字
    $black = imagecolorallocate($dst, 255, 255, 255);//字体颜色
    $str = "@花澜不见天";
    imagefttext($dst, 30, 0, 100, 100,$black, $font, $str);
    //输出图片
    list($dst_w, $dst_h, $dst_type) = getimagesize($dst_path);
    switch ($dst_type) {
        case 1://GIF
            header('Content-Type: image/gif');
            imagegif($dst);
            break;
        case 2://JPG
            header('Content-Type: image/jpeg');
            imagejpeg($dst);
            break;
        case 3://PNG
            header('Content-Type: image/png');
            imagepng($dst);
            break;
        default:
            break;
    }
    // 另存为 各种格式 这边就只列出一个
    imagejpeg($dst,$image_name[0].'_watermark.jpg');
    imagedestroy($dst);
