<?php
//设置响应头(编码格式)
header("Content-Type:text/html;charset=utf-8");
echo "/a/b/b.php<hr/>";

echo '$_SERVER["HTTP_HOST"]：【',$_SERVER['HTTP_HOST'],"】//当前请求的 Host:即域名信信息","<br/>";
echo '<br/>';
echo '$_SERVER["PHP_SELF"]：【<font color="#EE00EE">',$_SERVER['PHP_SELF'],"</font>】//<font color='green'>请求执行文件的</font><font color='red'>相对</font><font color='blue'>地址</font>","<br/>";
echo '$_SERVER["SCRIPT_NAME"]：【',$_SERVER['SCRIPT_NAME'],"】//<font color='green'>实际执行文件</font><font color='red'>相对</font><font color='blue'>地址</font>","<br/>";
echo '$_SERVER["SCRIPT_FILENAME"]：【',$_SERVER['SCRIPT_FILENAME'],"】//<font color='green'>实际执行文件的</font><font color='red'>绝对</font><font color='blue'>路径</font>。","<br/>";
echo '__FILE__ ：【<font color="#EE00EE">',__FILE__ ,"</font>】//<font color='green'>实际执行代码的</font><font color='red'>绝对</font><font color='blue'>路径</font>。","<br/>";
echo '<br/>';
echo '$_SERVER["REQUEST_URI"]：【',$_SERVER['REQUEST_URI'],"】//url请求中包括/和之后的所有内容。","<br/>";
echo '$_SERVER["QUERY_STRING"]：【',$_SERVER['QUERY_STRING'],"】//url请求中?之后的内容。","<br/>";
echo '<br/>';
echo '$_SERVER["DOCUMENT_ROOT"]：【',$_SERVER['DOCUMENT_ROOT'],"】//文档根目录。在服务器配置文件中定义","<br/>";
echo "<br/>";


echo 'basename($_SERVER["PHP_SELF"])：【<font color="#EE00EE">',basename($_SERVER['PHP_SELF']),"</font>】//<font color='green'>请求执行文件的</font>的<font color='blue'>文件名</font>","<br/>";
echo 'basename($_SERVER["SCRIPT_NAME"])：【',basename($_SERVER['SCRIPT_NAME']),"】//<font color='green'>实际执行文件</font>的<font color='blue'>文件名</font>","<br/>";
echo 'basename($_SERVER["SCRIPT_FILENAME"])：【',basename($_SERVER['SCRIPT_FILENAME']),"】//<font color='green'>实际执行文件</font>的<font color='blue'>文件名</font>","<br/>";
echo 'basename(__FILE__)：【<font color="#EE00EE">',basename(__FILE__),"</font>】//<font color='green'>实际执行代码</font>的<font color='blue'>文件名</font>","<br/>";
//echo 'basename($_SERVER["ORIG_SCRIPT_NAME"])：【',basename($_SERVER['ORIG_SCRIPT_NAME']),"】//如果PHP通过CGI来运行，这个变量的值就是/Php/Php.exe 如果Apache将PHP脚本作为模块来运行，该变量的值应该是/Phptest.php","<br/>";
echo "<br/>";


echo 'dirname($_SERVER["PHP_SELF"])：【<font color="#EE00EE">',dirname($_SERVER['PHP_SELF']),"</font>】//<font color='green'>请求执行文件的</font>的<font color='blue'>所在目录</font>","<br/>";
echo 'dirname($_SERVER["SCRIPT_NAME"])：【',dirname($_SERVER['SCRIPT_NAME']),"】//<font color='green'>实际执行文件</font>的<font color='blue'>所在目录</font>","<br/>";
echo 'dirname($_SERVER["SCRIPT_FILENAME"])：【',dirname($_SERVER['SCRIPT_FILENAME']),"】//<font color='green'>实际执行文件</font>的<font color='blue'>所在目录</font>","<br/>";
echo 'dirname(__FILE__)：【<font color="#EE00EE">',dirname(__FILE__),"</font>】//<font color='green'>实际执行代码</font>的<font color='blue'>所在目录</font>","<br/>";
//echo 'dirname($_SERVER["ORIG_SCRIPT_NAME"])：【',dirname($_SERVER['ORIG_SCRIPT_NAME']),"】//如果PHP通过CGI来运行，这个变量的值就是/Php/Php.exe 如果Apache将PHP脚本作为模块来运行，该变量的值应该是/Phptest.php","<br/>";
echo "<br/>";