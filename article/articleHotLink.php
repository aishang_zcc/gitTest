<!-- 
	要实现的功能：针对文章 进行对关键字添加链接 方便用户对一些字的进行快速查看了解 也方便搜索引擎的抓取数据
	冥想：要要针对的关键字数据 进行全文查找 然后替换成对应的url（生成的a标签元素），替换？那如果存在比较相似的 如 产业；工业产业；信息产业； 
	那我要怎么去替换。如果替换了产业，那后面包含的单独个体的怎么去处理？想了一会,好像就会出现这样的问题.那next。一些情况可能想不到了那就只能在解决问题中去寻找。
	操作 :   
	（1）对关键字进行排序 ？处理上面想到的关键字大包含小问题
	（2）把文章中所有的html元素 给取出来 和根据html进行分割字符串 ？
	（3）生成一个固定不会出现的字符串 来吧所有的html 元素进行替换 ？其实就是做上标记而已 便于之后
	（4）把需要的关键字生成a标签元素 和对应的标记 进行替换文章，然后在把文章中的标记替换成对应的a标签元素
 -->
<?php
/**
 * 给文章中添加关键词连接
 *
 * @author zcc
 * @date 2020-03-24
 */
class HotLink
{
	private $_content;
	private $_keywords;
	private $_times;

	function __construct($content, $keywords, $times = -1)
	{
		$this->content = $content;
		$this->keywords = $keywords;
		$this->times = $times;
	}
	/**
	 * ps ：1
	 * Time：2020/03/27 11:35:41
	 * @author zcc
	 * @param 参数类型
	 * @return 返回值类型
	 */
	public function make()
	{
		$areas = $this->content;
		$kWordArr = $this->sortKeyWord($this->keywords);
		$htmlTagArr = $this->getAllHtmlTag($areas); //提取所有html标签
		$splitStringArr = $this->getSplitString($areas); //没有html的

		$md5 = '(' . md5('zcc') . ')';
		$temContent = implode($md5, $splitStringArr);

		$rParray = [];
		//把要替换的关键词 进行转换 用md5标记 进行标记替换掉文章中对应的关键词 用于一下次循环替换关键词的url
		foreach ($kWordArr as $key => $v) {
			$rParray[$key]['preg'] = '/' . preg_quote($v[0]) . '/i';
			$rParray[$key]['url'] = '<a href="' . $v[1] . '" target="_blank" title="111" >' . $v[0] . '</a>';
			$rParray[$key]['md5'] = '{' . md5($v[0]) . '}';
			$temContent = preg_replace($rParray[$key]['preg'], $rParray[$key]['md5'], $temContent);
		}
		//把关键词的md5 替换成a标签
		foreach ($rParray as $key => $vv) {
			$temContent = str_replace($vv['md5'], $vv['url'], $temContent);
		}
		// 猜想：为什么我把上面的方法写了2个foreach去执行？
		$result = '';
		//把原来标记的html标签进行还原 那我第一个html标记 其实也就是用htmlTagArr中的第一个
		$temContentArr = explode($md5, $temContent);
		foreach ($temContentArr as $key => $value) {
			//temContentArr最后一个数组则为末尾
			if ($key + 1 == count($temContentArr)) {
				$result .= '';
			} else {
				$result .= $value . $htmlTagArr[$key];
			}
		}

		// $i = 0;
		// while (strpos($temContent,$md5)) {
		// 	$result = substr_replace($temContent,$htmlTagArr[$i],strpos($temContent,$md5),strlen($md5));
		// 	$i++;
		// }

		return $result;
	}


	/**
	 * 排序数据 长度最长的放在前面
	 *
	 * @param array $keyWordsArr
	 * @return array
	 * @author zcc
	 * @date 2020-03-27
	 */
	protected function sortKeyWord($keyWordsArr)
	{
		//使用usort 自定义函数排序 长度长的放在前面
		usort($keyWordsArr, function ($a, $b) {
			$f = strlen($a[0]);
			$s = strlen($b[0]);
			if ($f == $s) {
				return 0;
			}
			return $f > $s ? -1 : 1;
		});
		return $keyWordsArr;

		// 其他的方法 冒泡排序...
	}

	/**
	 * 提取出所有html标签
	 *
	 * @param string $content
	 * @author zcc
	 * @date 2020-03-27
	 */
	public function getAllHtmlTag($content)
	{

		// $preg = '/<a[^>]*>.*<\/a>';//可以单纯的匹配a标签的进行提取出来
		$preg = '/<a[^>]*>.*?<\/a>|<\/?[a-zA-Z]+[^>]*>/'; // 获取a标签的整个元素（<a href...>123</a>）和 其他元素的单个元素<p> <\p> <\br> 存进素组 
		preg_match_all($preg, $content, $matches);
		if (isset($matches[0])) {
			$htmlTagArray = $matches[0];
			return $htmlTagArray;
		}
		return [];
	}
	/**
	 * 根据html标签的进行切割字符串  正则要和提取html的一样 这样就能形成对应互补的两个数组
	 *
	 * @param string $content
	 * @author zcc
	 * @date 2020-03-27
	 */
	public function getSplitString($content)
	{
		return preg_split('/<a[^>]*>.*?<\/a>|<\/?[a-zA-Z]+[^>]*>/', $content);
	}
}
$keywords = [
	['社会主义', 'http://www.baidu.com'],
	['社会', 'http://www.baidu.com'],
	['民主', 'http://www.baidu.com'],
];
$content = "
当今社会富强、民主、文明、和谐,是我国社会主义现代化国家的建设目标<br>,
当今<a href='1111'>社会</a>富强、民主、文明、和谐,是我国<a href='1111'>社会主义</a>现代化国家的建设目标<br>
";
$calss = new HotLink($content, $keywords);
echo $calss->make();
die;
?>